<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
     	$dt = Carbon::now();
        $dateNow = $dt->toDateTimeString();
        DB::table('users')->insert([
        'name' 		 => 'Aku Admin',
        'email'		 => 'admin@gmail.com',
        'email_verified_at' =>'admin@gmail.com',
        'password' => bcrypt('12341234'),
        'level'		 => 'admin',
        'created_at' => $dateNow,
         'updated_at' => $dateNow
        ]);
        DB::table('users')->insert([
        'name' 		 => 'Aku Member',
        'email'		 => 'member@gmail.com',
        'email_verified_at' =>'member@gmail.com',
        'password' => bcrypt('12341234'),
        'level'		 => 'admin',
        'created_at' => $dateNow,
        'updated_at' => $dateNow
        ]);
        
	 }

}
